## Install environment
1. Open cmd as administrator
2. run `@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"`
3. Run `choco install -y nodejs.install python2 jdk8`
4. Download and install Android Studio
5. Open the System pane under System and Security in the Windows Control Panel, then click on Change settings.... Open the Advanced tab and click on Environment Variables.... Click on New... to create a new ANDROID_HOME user variable that points to the path to your Android SDK:
The SDK is installed, by default, at the following location:
`C:\Users\YOUR_USERNAME\AppData\Local\Android\Sdk`
6. Create a new JAVA_HOME user variable that points to the path to your java
7. Add PATH %JAVA_HOME%/bin
8. Create new virtual android device with API 28

## Create new project
1. Run `npx react-native init AwesomeProject`
2. Write `packagingOptions {
               pickFirst '**/libjsc.so'
           }` to android/app/build.gradle in android object
3. run android emulator

## Run existing project
1. Download project
2. Run `yarn`
3. open reactnative\node_modules\metro-config\src\defaults\blacklist.js replace
`var sharedBlacklist = [
    /node_modules[/\\]react[/\\]dist[/\\].*/,
    /website\/node_modules\/.*/,
    /heapCapture\/bundle\.js/,
    /.*\/__tests__\/.*/
  ]; `
  to 
 ` var sharedBlacklist = [
    /node_modules[\/\\]react[\/\\]dist[\/\\].*/,
    /website\/node_modules\/.*/,
    /heapCapture\/bundle\.js/,
    /.*\/__tests__\/.*/
  ];`
4. Run `yarn start`
5. Run `yarn android`

## FIX ERRORS
|Error|Resolve|
|---|---|
|>Task :app:transformDexArchiveWithExternalLibsDexMergerForDebug FAILED_ error|run `cd android && gradlew clean`|
|Wrong IP|**in emulator** - run `adb shell`, run `input keyevent 82`| 
|Wrong IP|**in real device** - shake your device| 
|>Task :app:mergeDexDebug|read https://rnfirebase.io/enabling-multidex |


## Build
Delete `packagingOptions {
               pickFirst '**/libjsc.so'
           }` from android/app/build.gradle in android object
* **Debug** apk (for connect to developer server in real device): run `cd android && gradlew assembleDebug`
* **Release** apk: run `cd android && gradlew assembleRelease`

## Generate certificate for firebase
```cd android && gradlew signingReport```

## Run emulator without AndroidStudio
1. open cmd
2. go to ```C:\Users\Username\AppData\Local\Android\Sdk\emulator```
3. run ```emulator -list-avds``` to view installed emulators
4. run ```emulator -avd EMULATOR_NAME -dns-server 8.8.8.8```