import React, {Component} from 'react';
import {Button, View, Text} from 'react-native';

export default class NavBar extends Component {

  render() {
    const {navigate} = this.props;
    return (
      <View>
        <Button
          title="To Start Screen"
          onPress={() => navigate('Start', {name: 'Jane'})}
        />
        <Button
          title="To A Screen"
          onPress={() => navigate('ScreenA')}
        />
        <Button
          title="To B Screen"
          onPress={() => navigate('ScreenB')}
        />
      </View>
    );
  }
}
