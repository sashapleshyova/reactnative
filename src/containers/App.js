import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import app from "../routes/app"
import {navigationRef} from "../constants/root-navigation";

const Stack = createStackNavigator();

function App() {
  return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator initialRouteName='start'>
            {
                app.map(route =>  <Stack.Screen
                    name={route.name}
                    component={route.component}
                    options={route.options}
                />)
            }
        </Stack.Navigator>
      </NavigationContainer>
  );
}

export default App;
